import React, { Component } from 'react';
import './App.css';

class FailstackInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      baseChance: 0.3,
      calcualtedChance: 0,
      result: '',
      failCount: 0,
      failCache: 0,
      tryAgain: false
    };

    this.handleInputChange  = this.handleInputChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleButtonClick  = this.handleButtonClick.bind(this);
    this.handleResetClick  = this.handleResetClick.bind(this);
    this.calculateChance    = this.calculateChance.bind(this);
  }

  handleInputChange(event) {
    var base = this.state.baseChance;
    var failstacks = event.target.value;

    this.setState({
      value: event.target.value,
      calcualtedChance: this.calculateChance(base, failstacks),
      failCount: 0,
      failCache: 0
    });
  }

  handleSelectChange(event) {
    var base = event.target.value;
    var failstacks = this.state.value;

    this.setState({
      baseChance: event.target.value,
      calcualtedChance: this.calculateChance(base, failstacks),
      failCount: 0,
      failCache: 0
    });
  }

  handleButtonClick(event) {
    var Random    = require("random-js");
    var engine    = Random.engines.mt19937().autoSeed();
    var chance    = parseFloat(this.state.calcualtedChance) / 100;
    var success   = Random.bool(chance)(engine);
    var message   = (success === true) ? 'Success!' : 'Failed';
    var failCount = this.state.failCount;
    var failCache = failCount + 1;
    var tryAgain  = this.state.tryAgain; 

    if (success) {
      tryAgain = true;
      failCount = 0;
    } else {
      failCache = 0;
      failCount++;
    }

    this.setState({result: message, failCount: failCount, failCache: failCache, tryAgain: tryAgain});
  }

  handleResetClick(event) {
    this.setState({tryAgain: false})
  }

  calculateChance(base, failstacks) {
    base = parseFloat(base);
    var softcap_fs, pre_softcap, post_sofcap, chance;

    switch(base) {
      case 25:
        softcap_fs = 18 //   base - pri accesory
        break;
      case 10:
        softcap_fs = 40 //   pri - duo accesory
        break;
      case 7.5:
        softcap_fs = 44 //   duo-tri accesory
        break;
      case 2.5:
        softcap_fs = 110 //  tri-tet accesory
        break;
      case 0.5:
        softcap_fs = 590 //  tet-pen accesory
        break;
      case 0.3:
        softcap_fs = 2323 // tet-pen weapon
        break;
      case 2:
        softcap_fs = 340 //  tri-tet weapon
        break;
      case 6.25:
        softcap_fs = 102 //  duo-tri weapon
        break;
      case 7.692:
        softcap_fs = 82 //   pri-duo weapon
        break;
      default:
        softcap_fs = failstacks
    }

    if (failstacks > softcap_fs) {
      pre_softcap = base + ( softcap_fs * (base / 10));
      post_sofcap = (failstacks - softcap_fs) * (base / 50);

      chance = pre_softcap + post_sofcap
    } else {
      chance = base + ( failstacks * (base / 10));
    }

    if (chance > 90.00) {
      chance = 90.00;
    }

    return chance.toFixed(2);
  }

  render() {
    return (
      <form>
        <label>
          Enhancement target:
          <select value={this.state.baseChance} onChange={this.handleSelectChange}>
            <option value="0.3">TET -> PEN (Weapon/Armor)</option>
            <option value="2">TRI -> TET (Weapon/Armor)</option>
            <option value="6.25">DUO -> TRI (Weapon/Armor)</option>
            <option value="7.692">PRI -> DUO (Weapon/Armor)</option>
            <option value="25">Base -> PRI (Accesory)</option>
            <option value="10">PRI -> DUO (Accesory)</option>
            <option value="7.5">DUO -> TRI (Accesory)</option>
            <option value="2.5">TRI -> TET (Accesory)</option>
            <option value="0.5">TET -> PEN (Accesory)</option>
          </select>
        </label>
        <br/>
        <label>
          Failstacks:
          <input className="failstackInput" value={this.state.value} onChange={this.handleInputChange} />
        </label>

        <h1>{this.state.calcualtedChance}%</h1>

        <button type="button" onClick={this.handleButtonClick} disabled={this.state.tryAgain}>
          Enchant using this percentage
        </button>

        <h2>{this.state.result}</h2>
        {this.state.tryAgain ? 'It only took you ' + this.state.failCache + ' tries' : ''}
        {this.state.failCount > 0 ? 'You failed with this percentage: ' + this.state.failCount + ' times' : ''}
        {this.state.tryAgain &&
          <div>
            <br/>
            <button type="button" onClick={this.handleResetClick}>Try again?</button>
          </div>
        }
        <br/>
      </form>
    );
  }
}


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <FailstackInput />
          <hr/>
          <small>Calculator made by Ksix, using formulas from reddit user @lions2lambs</small>
          <small>To simulate the enchant we are using <a href="https://www.wikiwand.com/en/Mersenne_Twister" target="_blank">Mersenne Twister</a>,
          which is a pseudo random number generator (RNG),
          commonly used in game development,
          so probably the one that uses BDO.</small>
          <a href="https://www.twitch.com/ksixx" className="lokthar">Watch me code stuff like this and play BDO at twitch.tv/ksixx</a>
        </header>
      </div>
    );
  }
}

export default App;
